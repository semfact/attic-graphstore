
package eu.semfact.attic.graphstore.tdb;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.tdb.TDBFactory;
import eu.semfact.attic.graphstore.StoreFactory;

public class MemoryTDBStoreFactory implements StoreFactory {

    @Override
    public Dataset getSession() {
        return TDBFactory.createDataset();
    }

    @Override
    public Dataset getStorage() {
        return TDBFactory.createDataset();
    }
    
}
