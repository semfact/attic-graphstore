
package eu.semfact.attic.graphstore.tdb;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.tdb.TDBFactory;
import eu.semfact.attic.graphstore.StoreFactory;

public class PersistentTDBStoreFactory implements StoreFactory {
    
    String session;
    String storage;
    
    public PersistentTDBStoreFactory() {
        this.session = "session";
        this.storage = "storage";
    }

    @Override
    public Dataset getSession() {
        return TDBFactory.createDataset(this.session);
    }

    @Override
    public Dataset getStorage() {
        return TDBFactory.createDataset(this.storage);
    }
    
}
